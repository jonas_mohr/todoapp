/// <reference path='../_all.ts' />

module todos {
	'use strict';

	export class TodoController {

		private todos: TodoItem[];

		public static $inject = [
			'$scope',
			'todoStorage'
		];

		constructor(
			private $scope: ITodoScope,
			private todoStorage: ITodoStorage
		) {
			this.todos = $scope.todos = todoStorage.get();

			$scope.newTodoText = '';
			$scope.newTodoDate = new Date;
			$scope.errorMessage = false;

			$scope.$watch('todos', () => this.onTodoEvent(), true);

			$scope.vm = this;

		}
		onTodoEvent() {
			this.todoStorage.put(this.todos);
		}

		addTodo() {
			var text : string = this.$scope.newTodoText.trim();
			if (!text.length || text.length < 10) {
				this.$scope.errorMessage = true
				return;
			} else {
				this.$scope.errorMessage = false
			}

			this.todos.push(new TodoItem(text, false, this.$scope.newTodoDate));
			this.$scope.newTodoText = '';
			this.$scope.newTodoDate = new Date
		}

		removeTodo(todoItem: TodoItem) {
			this.todos.splice(this.todos.indexOf(todoItem), 1);
		}

		checkDate(date) {
			var deadlineDate = new Date(date)
			var today = new Date()
			if (today.getDay() > deadlineDate.getDay()) {
				return 'deadline-overdue'
			}
		}
	}

}
