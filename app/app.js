/// <reference path='../_all.ts' />
var todos;
(function (todos) {
    'use strict';
    var TodoItem = (function () {
        function TodoItem(title, completed, deadline) {
            this.title = title;
            this.completed = completed;
            this.deadline = deadline;
        }
        return TodoItem;
    }());
    todos.TodoItem = TodoItem;
})(todos || (todos = {}));
/// <reference path='../_all.ts' />
/// <reference path='../_all.ts' />
/// <reference path='../_all.ts' />
var todos;
(function (todos_1) {
    'use strict';
    var TodoStorage = (function () {
        function TodoStorage() {
        }
        TodoStorage.prototype.get = function () {
            return JSON.parse(localStorage.getItem('todos') || '[]');
        };
        TodoStorage.prototype.put = function (todos) {
            localStorage.setItem('todos', JSON.stringify(todos));
        };
        return TodoStorage;
    }());
    todos_1.TodoStorage = TodoStorage;
})(todos || (todos = {}));
/// <reference path='../_all.ts' />
var todos;
(function (todos) {
    'use strict';
    var TodoController = (function () {
        function TodoController($scope, todoStorage) {
            var _this = this;
            this.$scope = $scope;
            this.todoStorage = todoStorage;
            this.todos = $scope.todos = todoStorage.get();
            $scope.newTodoText = '';
            $scope.newTodoDate = new Date;
            $scope.errorMessage = false;
            $scope.$watch('todos', function () { return _this.onTodoEvent(); }, true);
            $scope.vm = this;
        }
        TodoController.prototype.onTodoEvent = function () {
            this.todoStorage.put(this.todos);
        };
        TodoController.prototype.addTodo = function () {
            var text = this.$scope.newTodoText.trim();
            if (!text.length || text.length < 10) {
                this.$scope.errorMessage = true;
                return;
            }
            else {
                this.$scope.errorMessage = false;
            }
            this.todos.push(new todos.TodoItem(text, false, this.$scope.newTodoDate));
            this.$scope.newTodoText = '';
            this.$scope.newTodoDate = new Date;
        };
        TodoController.prototype.removeTodo = function (todoItem) {
            this.todos.splice(this.todos.indexOf(todoItem), 1);
        };
        TodoController.prototype.checkDate = function (date) {
            var deadlineDate = new Date(date);
            var today = new Date();
            if (today.getDay() > deadlineDate.getDay()) {
                return 'deadline-overdue';
            }
        };
        return TodoController;
    }());
    TodoController.$inject = [
        '$scope',
        'todoStorage'
    ];
    todos.TodoController = TodoController;
})(todos || (todos = {}));
/// <reference path='_all.ts' />
/**
 * The main module.
 *
 * @type {angular.Module}
 */
var todos;
(function (todos) {
    'use strict';
    var todoApp = angular.module('todoApp', [])
        .controller('todoController', todos.TodoController)
        .service('todoStorage', todos.TodoStorage);
})(todos || (todos = {}));
/// <reference path='libs/jquery/jquery.d.ts' />
/// <reference path='libs/angular/angular.d.ts' />
/// <reference path='models/TodoItem.ts' />
/// <reference path='interfaces/ITodoScope.ts' />
/// <reference path='interfaces/ITodoStorage.ts' />
/// <reference path='services/TodoStorage.ts' />
/// <reference path='controllers/TodoController.ts' />
/// <reference path='app.ts' /> 
//# sourceMappingURL=app.js.map