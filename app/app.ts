/// <reference path='_all.ts' />

/**
 * The main module.
 *
 * @type {angular.Module}
 */

module todos {
    'use strict';

    var todoApp = angular.module('todoApp', [])
            .controller('todoController', TodoController)
            .service('todoStorage', TodoStorage);
}