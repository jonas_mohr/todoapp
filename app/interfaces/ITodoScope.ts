/// <reference path='../_all.ts' />

module todos {
	export interface ITodoScope extends ng.IScope {
		todos: TodoItem[];
		newTodoText: string;
		newTodoDate: Date;
		vm: TodoController;
		errorMessage: boolean;
	}
}
