/// <reference path='../_all.ts' />

module todos {
    'use strict';

    export class TodoStorage implements ITodoStorage {

        get (): TodoItem[] {
            return JSON.parse(localStorage.getItem('todos') || '[]');
        }

        put(todos: TodoItem[]) {
            localStorage.setItem('todos', JSON.stringify(todos));
        }
    }
}